DELETE From Users;
-- ADMIN/123789
ALTER SEQUENCE users_id_seq RESTART WITH 1;
INSERT INTO Users (UserName, Password, PassSalt, DisplayName, RegisterTime, ApprovedTime,ApprovedBy, Description) values ('Admin', 'Es7WVgNsJuELwWK8daCqufUBknCsSC0IYDphQZAiGOo=', 'W5vpBEOYRGHkQXatN0t+ECM/U8cHDuEgrq56+zZBk4J481xH', 'Administrator', now(), now(), 'system', '系统默认创建');

DELETE From Dicts;
ALTER SEQUENCE dicts_id_seq RESTART WITH 1;
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('菜单', '系统菜单', '0', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('菜单', '外部菜单', '1', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('应用程序', '未设置', '0', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('网站设置', '网站标题', '后台管理系统', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('网站设置', '网站页脚', '2016 © 通用后台管理系统', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('系统通知', '用户注册', '0', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('系统通知', '程序异常', '1', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('系统通知', '数据库连接', '2', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('通知状态', '未处理', '0', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('通知状态', '已处理', '1', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('处理结果', '同意', '0', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('处理结果', '拒绝', '1', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('消息状态', '未读', '0', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('消息状态', '已读', '1', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('消息标签', '一般', '0', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('消息标签', '紧要', '1', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('头像地址', '头像路径', '~/images/uploader/', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('头像地址', '头像文件', 'default.jpg', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('网站样式', '蓝色样式', 'blue.css', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('网站样式', '黑色样式', 'black.css', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('当前样式', '使用样式', 'blue.css', 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('网站设置', '前台首页', '~/Home/Index', 0);

DELETE FROM Navigations;
ALTER SEQUENCE navigations_id_seq RESTART WITH 1;
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '后台管理', 10, 'fa fa-gear', '~/Admin/Index', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '个人中心', 20, 'fa fa-suitcase', '~/Admin/Profiles', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '返回前台', 30, 'fa fa-hand-o-left', '~/Home/Index', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '网站设置', 40, 'fa fa-fa', '~/Admin/Settings', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '菜单管理', 50, 'fa fa-dashboard', '~/Admin/Menus', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '用户管理', 60, 'fa fa-user', '~/Admin/Users', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '角色管理', 70, 'fa fa-sitemap', '~/Admin/Roles', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '部门管理', 80, 'fa fa-bank', '~/Admin/Groups', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '字典表维护', 90, 'fa fa-book', '~/Admin/Dicts', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '站内消息', 100, 'fa fa-envelope', '~/Admin/Messages', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '任务管理', 110, 'fa fa fa-tasks', '~/Admin/Tasks', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '通知管理', 120, 'fa fa-bell', '~/Admin/Notifications', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '系统日志', 130, 'fa fa-gears', '#', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (currval('navigations_id_seq') - 1, 0, '操作日志', 10, 'fa fa-edit', '~/Admin/Logs', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (currval('navigations_id_seq') - 2, 0, '登录日志', 20, 'fa fa-user-circle-o', '~/Admin/Logins', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '在线用户', 140, 'fa fa-users', '~/Admin/Online', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '程序异常', 150, 'fa fa-cubes', '~/Admin/Exceptions', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (0, '工具集合', 160, 'fa fa-gavel', '#', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (currval('navigations_id_seq') - 1, '客户端测试', 10, 'fa fa-wrench', '~/Admin/Mobile', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (currval('navigations_id_seq') - 2, 'API文档', 10, 'fa fa-wrench', '~/swagger', '0');
INSERT INTO Navigations (ParentId, Name, "order", Icon, Url, Category) VALUES (currval('navigations_id_seq') - 3, '图标集', 10, 'fa fa-dashboard', '~/Admin/FAIcon', '0');

DELETE FROM Groups;
ALTER SEQUENCE groups_id_seq RESTART WITH 1;
INSERT INTO Groups (ID, GroupName, Description) VALUES (1, 'Admin', '系统默认组');

DELETE FROM Roles;
ALTER SEQUENCE roles_id_seq RESTART WITH 1;
INSERT INTO Roles (RoleName, Description) VALUES ('Administrators', '系统管理员');
INSERT INTO Roles (RoleName, Description) VALUES ('Default', '默认用户，可访问前台页面');

DELETE FROM RoleGroup;
INSERT INTO RoleGroup (RoleID, GroupID) VALUES (1, 1);

DELETE FROM UserGroup;
INSERT INTO UserGroup (UserID, GroupID) VALUES (1, 1);

DELETE FROM UserRole;
INSERT INTO UserRole (UserID, RoleID) VALUES (1, 1);
INSERT INTO UserRole (UserID, RoleID) VALUES (1, 2);

DELETE FROM NavigationRole;
INSERT INTO NavigationRole (NavigationID, RoleID) SELECT ID, 1 FROM navigations;
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (1, 2);
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (2, 2);
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (3, 2);
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (10, 2);
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (16, 2);
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (17, 2);
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (18, 2);
INSERT INTO NavigationRole (NavigationID, RoleID) VALUES (19, 2);

-- Client Data
Delete From Dicts Where Category = '应用程序' and Code = '2';
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('应用程序', '测试平台', 2, 0);
INSERT INTO Dicts (Category, Name, Code, Define) VALUES ('应用首页', 2, 'http://localhost:49185/', 0);

Delete From Dicts Where Category = '测试平台';
Insert into Dicts (Category, Name, Code, Define) values ('测试平台', '网站标题', 'BA Client', 1);
Insert into Dicts (Category, Name, Code, Define) values ('测试平台', '网站页脚', '通用后台管理测试平台', 1);
Insert into Dicts (Category, Name, Code, Define) values ('测试平台', '个人中心地址', 'http://localhost:50852/Admin/Profiles', 1);
Insert into Dicts (Category, Name, Code, Define) values ('测试平台', '系统设置地址', 'http://localhost:50852/Admin/Index', 1);

Delete from Navigations where Application = '2';
INSERT into Navigations (ParentId, Name, "order", Icon, Url, Category, Application) VALUES (0, '首页', 10, 'fa fa-fa', '~/Home/Index', '1', 2);
INSERT into Navigations (ParentId, Name, "order", Icon, Url, Category, Application) VALUES (0, '测试页面', 20, 'fa fa-fa', '#', '1', 2);
INSERT into Navigations (ParentId, Name, "order", Icon, Url, Category, Application) VALUES (currval('navigations_id_seq') - 1, '关于', 10, 'fa fa-fa', '~/Home/About', '1', 2);

-- 菜单授权
DELETE FROM NavigationRole Where NavigationID in (Select ID From Navigations Where Application = '2');
INSERT INTO NavigationRole (NavigationID, RoleID) SELECT ID, 2 FROM Navigations Where Application = '2';
